module.exports = {
  content: ['./index.html', './src/**/*.{jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'brand-rocket': '#8257E5',
        'brand-hover': '#996DFF',
        'on-brand-color': '#fff',
      },
    },
  },
  plugins: [],
};
