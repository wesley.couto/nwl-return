import { ChatTeardropDots } from 'phosphor-react';
import { Popover } from '@headlessui/react';

export const Widget = () => {
  return (
    <Popover className="absolute bottom-5 right-5">
      <Popover.Panel>Teste</Popover.Panel>

      <Popover.Button className="bg-brand-rocket rounded-full px-3 h-12 text-on-brand-color flex items-center group">
        <ChatTeardropDots size={24} />

        <span className="max-w-0 overflow-hidden group-hover:max-w-xs transition-all duration-500 ease-linear">
          Feedback
        </span>
      </Popover.Button>
    </Popover>
  );
};
