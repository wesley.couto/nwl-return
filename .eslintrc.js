module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "airbnb",
        "plugin:@typescript-eslint/recommended",
        "plugin:prettier/recommended",
        "plugin:import/recommended",
        "plugin:import/typescript",
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-hooks",
        "@typescript-eslint",
        "prettier"
    ],
    "rules": {
        "max-len": [
            "warn",
            {
                "code": 90,
                "tabWidth": 2,
                "comments": 80,
                "ignoreComments": false,
                "ignoreTrailingComments": true,
                "ignoreUrls": true,
                "ignoreStrings": true,
                "ignoreTemplateLiterals": true,
                "ignoreRegExpLiterals": true
            }
        ],
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "react/jsx-filename-extension": [
            1,
            {
                "extensions": [
                    ".tsx"
                ]
            }
        ],
        "import/prefer-default-export": "off",
        "camelcase": "off",
        "@typescript-eslint/ban-types": "off",
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                "ts": "never",
                "tsx": "never"
            }
        ],
        "no-use-before-define": "off",
        "prettier/prettier": [
            "error",
            {
                "endOfLine": "auto"
            }
        ],
        "react/prop-types": "off",
        "react/jsx-props-no-spreading": "off",
        "no-shadow": "off",
        "@typescript-eslint/no-shadow": "error",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "react/require-default-props": "off",
        "react/react-in-jsx-scope": "off",
        "no-underscore-dangle": 0,
        "react/function-component-definition": [
            "error",
            {
                "namedComponents": "arrow-function",
                "unnamedComponents": "arrow-function",
                "statelessComponents": "arrow-function"
            }
        ]
    },
    "settings": {
        "import/resolver": {
            "typescript": {},
        },
        "eslint-import-resolver-custom-alias": {
            "alias": {
                "@c": "./src/components",
            },
            "extensions": ["ts", "tsx", "js", "jsx"]
        }
    }
}
